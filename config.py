cluster = 'testcluster'
puppet_source = 'git@bitbucket.org:rimey/hello-ec2-puppetboot.git'

region = 'us-east-1'
zones = ['us-east-1a', 'us-east-1b']
total_instances = 2

instance = {
    'image_id': 'ami-a0ba68c9',     # us-east-1 oneiric i386 ebs 20120222
    'instance_type': 't1.micro',
    'key_name': 'awskey',
    'security_groups': ['default'],
}
