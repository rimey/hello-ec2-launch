#!/usr/bin/python

import time
from string import Template

import boto.ec2

import config

def get_script(filename='user-data-script.sh'):
    """Return the user data script, customized as per the configuration."""
    template = open(filename).read()
    return Template(template).substitute(
        puppet_source=config.puppet_source,
        vcs_known_hosts=open('vcs_keys/known_hosts').read().strip(),
        vcs_deploy_public=open('vcs_keys/id_rsa.pub').read().strip(),
        vcs_deploy_private=open('vcs_keys/id_rsa').read().strip(),
    )

def launch():
    connection = boto.ec2.connect_to_region(config.region)

    print 'Launching instance...'
    reservation = connection.run_instances(
        user_data=get_script(),
        **config.instance
    )

    instances = reservation.instances
    connection.create_tags([i.id for i in instances], {
        'role': config.cluster,
    })

    assert len(instances) == 1
    instance = instances[0]

    while instance.state != 'running':
        time.sleep(1)
        instance.update()

    print instance.public_dns_name

role_synonyms = ['roles', 'role', 'aws:autoscaling:groupName']

def get_hosts():
    role = config.cluster
    connection = boto.ec2.connect_to_region(config.region)

    hosts = []
    for reservation in connection.get_all_instances():
        for instance in reservation.instances:
            if instance.state == 'running':
                for key in role_synonyms:
                    value = instance.tags.get(key)
                    if value and role in value.split(','):
                        hosts.append(instance.public_dns_name)
                        break
    return hosts

if __name__ == '__main__':
    launch()
