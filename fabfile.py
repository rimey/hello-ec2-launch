from fabric.api import cd, env, run, sudo, task, roles

import config
import launch_cluster
import launch_instance

env.user = 'ubuntu'
env.roledefs['instances'] = launch_instance.get_hosts

@task
def up():
    launch_cluster.launch()

@task
def down():
    launch_cluster.shutdown()

@task
@roles('instances')
def tail(lines=10):
    run('tail /var/log/nginx/access.log --lines=%s' % lines)

@task
@roles('instances')
def update():
    with cd('/etc/puppet'):
        sudo('git fetch')
        sudo('git reset --hard origin')
        sudo('puppet apply /etc/puppet/manifests/init.pp')
